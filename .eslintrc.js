module.exports = {
  env: {
    browser: true,
    es6: true,
    jquery: true
  },
  extends: ['standard'],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  parser: 'babel-eslint',
  rules: {
    semi: ['error', 'always'],
    'space-before-function-paren': 'off',
    'spaced-comment': 2
  }
};
