/*
 * @Description: gulpfile.js文件
 * @Author: yao yu qing
 * @Date: 2020-11-10 13:55:40
 * @LastEditTime: 2021-03-12 18:33:40
 * @LastEditors: yyq
 * @FilePath: \gulp4-webpack4-demo\gulpfile.js
 */
const baseConf = require('./build/webpack.base.config');
const devServerOptions = require('./config/dev-server-config');
const merge = require('webpack-merge');
// 让日志更加友好
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
// 查找开放端口或域接字的简单工具
const portfinder = require('portfinder');
const webpack = require('webpack');
// const webpackStream = require("webpack-stream");
const WebpackDevServer = require('webpack-dev-server');
const ora = require('ora');
const chalk = require('chalk');

let config, devConf;

function webpackDevConf() {
  config = require('./build/webpack.dev.config');
  devConf = merge(baseConf, config);
  portfinder.basePort = devServerOptions.port;
  portfinder
    .getPortPromise()
    .then(port => {
      // add port to devServer config
      devServerOptions.port = port;
      // Add FriendlyErrorsPlugin
      devConf.plugins.push(
        new FriendlyErrorsPlugin({
          compilationSuccessInfo: {
            messages: [
              `Your application is running here: http://${devServerOptions.host}:${port}`
            ]
          },
          clearConsole: true
        })
      );
      WebpackDevServer.addDevServerEntrypoints(devConf, devServerOptions);
      const compiler = webpack(devConf);
      const server = new WebpackDevServer(compiler, devServerOptions);
      server.listen(devServerOptions.port, devServerOptions.host);
    })
    .catch(err => {
      console.log(chalk.red(err));
    });
}

function webpackProductConf(done) {
  config = require('./build/webpack.prod.config');
  const spinner = ora('building for production...');
  spinner.start();
  const compiler = webpack(merge(baseConf, config));
  compiler.run((err, stats) => {
    spinner.stop();
    if (err) throw err;
    process.stdout.write(
      stats.toString({
        colors: true,
        modules: false,
        children: false,
        chunks: false,
        chunkModules: false
      }) + '\n\n'
    );

    if (stats.hasErrors()) {
      console.log(chalk.red('  Build failed with errors.\n'));
      process.exit(1);
    }

    console.log(chalk.cyan('  Build complete.\n'));
    console.log(
      chalk.yellow(
        '  Tip: built files are meant to be served over an HTTP server.\n' +
          "  Opening index.html over file:// won't work.\n"
      )
    );
  });

  done();
}

const { series, src } = require('gulp');
const gulpif = require('gulp-if');
var minimist = require('minimist');

var knownOptions = {
  string: 'env',
  default: { env: process.env.NODE_ENV || 'production' }
};

var options = minimist(process.argv.slice(2), knownOptions);

function test(done) {
  webpackDevConf();
  done();
}

var line = {
  test: {
    host: '47.98.216.234',
    user: 'sansiri',
    pass: 'sesXT6AxCBj8W4tH',
    port: 21
  }
};

exports.default = series(webpackDevConf);
exports.dev = series(webpackDevConf);
exports.prod = series(webpackProductConf);

exports.test = series(test);
