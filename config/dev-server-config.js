/*
 * @Description: dev-server-config
 * @Author: yao yu qing
 * @Date: 2020-11-12 11:19:55
 * @LastEditTime: 2021-03-12 18:27:06
 * @LastEditors: yyq
 * @FilePath: \gulp4-webpack4-demo\config\dev-server-config.js
 */
module.exports = {
  hot: true,
  useLocalIp: true,
  host: '0.0.0.0',
  port: 8080,
  // open: true,
  // writeToDisk: true, //写入到磁盘
  compress: true,
  publicPath: '/',
  contentBase: false,
  // stats: 'errors-only', // 此选项使可以精确控制显示哪些捆绑软件信息。
  // noInfo: true, // 告诉开发服务器禁止显示诸如 Webpack 捆绑包信息之类的消息。 错误和警告仍将显示。
  // quiet: true, // 如果使用webpack-dev-server，需要设为true，禁止显示devServer的console信息
  openPage: 'index.html', // 指定默认启动浏览器时打开的页面
  index: 'index.html', // 指定首页位置
  // hotOnly: true, // HMR 不自动刷新
  inline: true, // 页面刷新
  noInfo: true, // webpack-dashboard
  // notifyOnErrors: true, // 跨平台错误提示
  overlay: true, // 在浏览器中显示错误
  before: function (app, server) {
    server._watch(`src/**.art`);
    server._watch(`src/*/**.art`);
  }
};
