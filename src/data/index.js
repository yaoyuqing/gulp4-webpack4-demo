module.exports = {
  lang: {
    home: '首页',
    address: '地址',
    zipcode: '邮编',
    tel: '电话',
    mobile: '手机',
    fax: '传真',
    email: '邮箱',
    name: '姓名',
    content: '内容',
    submit: '提交',
    all: '全部',
    details: '详情',
    more: '更多',
    correlation: '相关内容',
    focus: '关注',
    consultation: '在线咨询',
    phone: '联系电话',
    site: '您的位置',
    support: '技术支持',
    code: '验证码',
    enterCode: '请输入验证码'
  },
  jsMain: 'index',
  ftpconf: {
    jspath: 'http://code.uemo.net/p/mo5/org.1494058893.js',
    csslibpath: '/templates/ue_content/templates/css/lib2.css',
    sliderv: 2
  },
  is_index: 1,
  SCRIPT_NAME: '/index.php',
  TSiteData: {
    uid: 14848,
    siteagent: 'pc',
    headersid: 0,
    lang: 'cn',
    seotitle: '五号模板',
    title: 'mo005_XXXXX',
    description: '茶舍工作室',
    siteurl: '//mo005-14848.mo5.line1.jsmo.xin/',
    sitetplurl:
      '//mo005-14848.mo5.line1.jsmo.xin/templates/ue_content/templates/',
    copyid: 14848,
    ucssurl: '//resources.jsmo.xin/templates/upload/14848/14848.css',
    form_token: '604ae3b4903cf',
    logo:
      '//resources.jsmo.xin/templates/upload/14848/201904/1554363269470.png',
    logo2x:
      '//resources.jsmo.xin/templates/upload/14848/201904/1554363273819.png',
    content_show: '',
    content_data: '',
    beian: '',
    footjs: '//resources.jsmo.xin/templates/upload/14843/14848.js',
    footlogo: {
      logo: '',
      url: ''
    },
    keywords: '喝茶 茶叶 茶饼 特产 作用 浓茶 铁观音',
    site: {
      id: 14848,
      msuid: 3,
      expiredtime: 1870582862,
      lineid: 1,
      copyid: 14848,
      seo: 0
    }
  },
  TOnlineLXData: {
    tel: ['86-1524328'],
    qq: ['40080000'],
    email: 'touch@studio.com',
    weixin:
      '//resources.jsmo.xin/templates/upload/14848/201910/1570695534716.jpg'
  },
  TPluginData: {
    player111: ''
  },
  TPageData: {
    banner:
      '//resources.jsmo.xin/templates/upload/14848/201905/1557470150729.png',
    meta: {
      singlescreen: 1,
      ease: '',
      control: 1,
      scroll: {
        speed: '',
        ease: ''
      }
    },
    nav: [
      {
        title: '首页',
        target: '_self',
        url: '//mo005-14848.mo5.line1.jsmo.xin/',
        active: 'active'
      },
      {
        title: '案例',
        target: '_self',
        url: '//mo005-14848.mo5.line1.jsmo.xin/list/id/165184/'
      },
      {
        title: '服务',
        target: '_self',
        url: '//mo005-14848.mo5.line1.jsmo.xin/list/id/165195/'
      },
      {
        title: '热销',
        target: '_self',
        url: '//mo005-14848.mo5.line1.jsmo.xin/list/id/168657/'
      },
      {
        title: '新闻',
        target: '_self',
        url: '//mo005-14848.mo5.line1.jsmo.xin/list/id/165190/'
      },
      {
        title: '关于',
        sub: [
          {
            title: '集团概况',
            target: '_self',
            url: '//mo005-14848.mo5.line1.jsmo.xin/page/about/'
          },
          {
            title: '团队',
            target: '_self',
            url: '//mo005-14848.mo5.line1.jsmo.xin/list/id/165193/'
          },
          {
            title: '视频',
            target: '_self',
            url: '//mo005-14848.mo5.line1.jsmo.xin/list/id/165196/'
          },
          {
            title: '合作伙伴',
            target: '_self',
            url: '//mo005-14848.mo5.line1.jsmo.xin/list/id/165192/'
          },
          {
            title: '定制专场',
            target: '_self',
            url: '//mo005-14848.mo5.line1.jsmo.xin/page/266363/'
          }
        ]
      },
      {
        title: '联系',
        target: '_self',
        url: '//mo005-14848.mo5.line1.jsmo.xin/page/contact/'
      }
    ],
    content: [
      {
        mname: 'slider',
        title: 'Slider',
        subtitle: '',
        ibg: '',
        content: [
          {
            title: '',
            subtitle: '',
            image:
              '//resources.jsmo.xin/templates/upload/14848/201904/1554362887178.jpg',
            url: '',
            meta: {
              talign: 'left',
              valign: '',
              ubb: 1
            },
            thumb:
              '//resources.jsmo.xin/templates/upload/14848/201904/1554362887178_80x80.jpg'
          },
          {
            title: '茶文化·归茶',
            subtitle: '山青水秀扬临海,高香醇爽羊岩茶',
            image:
              '//resources.jsmo.xin/templates/upload/14848/201904/1554363075400.jpg',
            url: '',
            meta: {
              talign: '',
              valign: '',
              ubb: 0
            },
            thumb:
              '//resources.jsmo.xin/templates/upload/14848/201904/1554363075400_80x80.jpg'
          },
          {
            title: '茶文化 · 浮沉',
            subtitle: '',
            image:
              '//resources.jsmo.xin/templates/upload/14848/201904/1555053292720.jpg',
            url: '',
            meta: {
              talign: '',
              valign: '',
              ubb: 0
            },
            thumb:
              '//resources.jsmo.xin/templates/upload/14848/201904/1555053292720_80x80.jpg'
          }
        ],
        meta: {
          height: 0,
          auto: 1,
          mode: 1,
          pause: 4,
          speed: 1,
          mask: 0,
          thumb: 0
        }
      },
      {
        mname: 'list',
        title: '案例',
        subtitle: 'Case',
        ibg: '',
        content: {
          title: '案例',
          subtitle: 'Case',
          type: 'project',
          keywords: '',
          url: '//mo005-14848.mo5.line1.jsmo.xin/list/id/165184/',
          category: [
            {
              id: '165184',
              title: '全部',
              url: '//mo005-14848.mo5.line1.jsmo.xin/list/id/165184/',
              active: 'active'
            },
            {
              id: '165185',
              title: '自饮选茶',
              url: '//mo005-14848.mo5.line1.jsmo.xin/list/id/165185/'
            },
            {
              id: '165186',
              title: '送礼选购',
              url: '//mo005-14848.mo5.line1.jsmo.xin/list/id/165186/'
            },
            {
              id: '165187',
              title: '精选品牌',
              url: '//mo005-14848.mo5.line1.jsmo.xin/list/id/165187/'
            },
            {
              id: '165188',
              title: '限量特价',
              url: '//mo005-14848.mo5.line1.jsmo.xin/list/id/165188/'
            }
          ],
          data: {
            list: [
              {
                id: 800011,
                title: '罗布麻茶的功效',
                subtitle: '携带方便 物美价廉 可以做餐点',
                thumbnail:
                  '//resources.jsmo.xin/templates/upload/14848/201904/1554371531949.png',
                keywords: '',
                description: '',
                tags: [
                  {
                    title: '送礼选购',
                    url:
                      '//mo005-14848.mo5.line1.jsmo.xin/list/id/165184/tag/%25E9%2580%2581%25E7%25A4%25BC%25E9%2580%2589%25E8%25B4%25AD/'
                  }
                ],
                usetdate: '2019-04-04',
                url: '//mo005-14848.mo5.line1.jsmo.xin/list/post/800011/',
                usetdateAry: ['04-04', '2019'],
                cattitle: '送礼选购'
              },
              {
                id: 800009,
                title: '大红袍茶叶武夷岩茶直销',
                subtitle: 'Camellia sinensis (L.) O. Ktze',
                thumbnail:
                  '//resources.jsmo.xin/templates/upload/14848/201904/1554370289520.png',
                keywords: '',
                description:
                  '元代还在武夷山设立了“焙局”、“御茶园”,专门采制贡茶',
                tags: [
                  {
                    title: '送礼选购',
                    url:
                      '//mo005-14848.mo5.line1.jsmo.xin/list/id/165184/tag/%25E9%2580%2581%25E7%25A4%25BC%25E9%2580%2589%25E8%25B4%25AD/'
                  }
                ],
                usetdate: '2019-04-04',
                url: '//mo005-14848.mo5.line1.jsmo.xin/list/post/800011/',
                usetdateAry: ['04-04', '2019'],
                cattitle: '送礼选购'
              },
              {
                id: 799782,
                title: '人参乌龙茶兰贵人',
                subtitle: '足不出户&quot;美食&quot;轻松送到家!',
                thumbnail:
                  '//resources.jsmo.xin/templates/upload/14848/201904/1554368167632.png',
                keywords: '',
                description:
                  '人参茶以台湾高山乌龙茶，配以西洋参叶粉调制而成，外形墨绿紧结，呈颗粒状；颗粒均匀,入口清香扑鼻，舌底生津，回味无穷。茶香袅绕飘飘，茶汤金莹剔透，味甘爽甜，饮之参味在先，茶韵其后，回味 甘醇。生津提神，排体毒，助消化，养身怡神，平气护腑。',
                tags: [
                  {
                    title: '送礼选购',
                    url:
                      '//mo005-14848.mo5.line1.jsmo.xin/list/id/165184/tag/%25E9%2580%2581%25E7%25A4%25BC%25E9%2580%2589%25E8%25B4%25AD/'
                  },
                  {
                    title: '送礼选购',
                    url:
                      '//mo005-14848.mo5.line1.jsmo.xin/list/id/165184/tag/%25E9%2580%2581%25E7%25A4%25BC%25E9%2580%2589%25E8%25B4%25AD/'
                  }
                ],
                usetdate: '2019-04-04',
                url: '//mo005-14848.mo5.line1.jsmo.xin/list/post/800011/',
                usetdateAry: ['04-04', '2019'],
                cattitle: '送礼选购'
              }
            ]
          }
        },
        meta: {
          limit: 12,
          slider: {
            num: 4,
            loop: 1
          },
          wide: 0,
          ibg: {
            mode2: 'center'
          }
        }
      }
    ]
  }
};
